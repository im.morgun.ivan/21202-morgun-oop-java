package com.example.calculator.models.operations.math_operations;

import com.example.calculator.exceptions.EmptyStackException;
import com.example.calculator.exceptions.NegativeSqrtValueException;
import com.example.calculator.exceptions.OperationArgsValidationException;
import com.example.calculator.models.containers.CalculatorContext;
import com.example.calculator.models.operations.EmptyArgsOperation;

public class SqrtOperation extends EmptyArgsOperation<Double> {

    @Override
    public void apply(CalculatorContext<Double> context, String argsLine) throws EmptyStackException, OperationArgsValidationException, NegativeSqrtValueException {
        validateArgs(argsLine);
        Double a;
        try {
            a = context.stackedValues.pop();
        } catch (Exception e) {
            throw new EmptyStackException(this.getClass().getName());
        }
        if (a < 0) {
            throw new NegativeSqrtValueException(getClass().getName());
        }

        context.stackedValues.add(Math.sqrt(a));
    }

}
