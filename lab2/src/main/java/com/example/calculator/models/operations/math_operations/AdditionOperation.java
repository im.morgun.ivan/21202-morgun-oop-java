package com.example.calculator.models.operations.math_operations;

import com.example.calculator.exceptions.EmptyStackException;
import com.example.calculator.exceptions.OperationArgsValidationException;
import com.example.calculator.models.containers.CalculatorContext;
import com.example.calculator.models.operations.EmptyArgsOperation;

public class AdditionOperation extends EmptyArgsOperation<Double> {

    @Override
    public void apply(CalculatorContext<Double> context, String argsLine) throws EmptyStackException, OperationArgsValidationException {
        validateArgs(argsLine);
        
        if (context.stackedValues.size() < 2) {
            throw new EmptyStackException(this.getClass().getName());
        }
        Double a = context.stackedValues.pop();
        Double b = context.stackedValues.pop();
        context.stackedValues.push(a + b);
    }

}
