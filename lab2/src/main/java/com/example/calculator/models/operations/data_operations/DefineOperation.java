package com.example.calculator.models.operations.data_operations;

import java.util.ArrayList;
import java.util.List;

import com.example.calculator.exceptions.OperationArgsValidationException;
import com.example.calculator.models.containers.CalculatorContext;
import com.example.calculator.models.operations.AbstractOperation;

public class DefineOperation extends AbstractOperation<Double> {
    
    @Override
    protected List<Class<?>> getArgsFormat() {
        List<Class<?>> format = new ArrayList<>();
        format.add(String.class);
        format.add(Double.class);
        return format;
    }

    @Override
    public void apply(CalculatorContext<Double> context, String argsLine) throws OperationArgsValidationException {
        List<Object> args = validateArgs(argsLine);
        context.definedValues.put((String) args.get(0), (Double) args.get(1));
    }

}
