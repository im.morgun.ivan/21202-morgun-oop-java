package com.example.calculator.models.operations.data_operations;

import java.util.ArrayList;
import java.util.List;

import com.example.calculator.exceptions.NoDefinedValueException;
import com.example.calculator.exceptions.OperationArgsValidationException;
import com.example.calculator.models.containers.CalculatorContext;
import com.example.calculator.models.operations.AbstractOperation;

public class PushOperation extends AbstractOperation<Double> {
    
    @Override
    protected List<Class<?>> getArgsFormat() {
        List<Class<?>> format = new ArrayList<>();
        format.add(String.class);
        return format;
    }

    @Override
    public void apply(CalculatorContext<Double> context, String argsLine) throws OperationArgsValidationException, NoDefinedValueException {
        List<Object> args = validateArgs(argsLine);
        if (context.definedValues.get((String) args.get(0)) == null) {
            throw new NoDefinedValueException(getClass().getName(), (String) args.get(0));
        }
        Double value = context.definedValues.get((String) args.get(0));
        context.stackedValues.push(value);
    }

}
