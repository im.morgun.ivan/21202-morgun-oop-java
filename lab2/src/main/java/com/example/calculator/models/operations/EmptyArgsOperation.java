package com.example.calculator.models.operations;

import java.util.Arrays;
import java.util.List;

public abstract class EmptyArgsOperation<ValueType> extends AbstractOperation<ValueType> {
    
    @Override
    protected List<Class<?>> getArgsFormat() {
        return Arrays.asList();
    }
}
