package com.example.calculator.models.operations.data_operations;

import com.example.calculator.exceptions.EmptyStackException;
import com.example.calculator.exceptions.OperationArgsValidationException;
import com.example.calculator.models.containers.CalculatorContext;
import com.example.calculator.models.operations.EmptyArgsOperation;

public class PopOperation extends EmptyArgsOperation<Double> {

    @Override
    public void apply(CalculatorContext<Double> context, String argsLine) throws OperationArgsValidationException, EmptyStackException {
        validateArgs(argsLine);
        if (context.stackedValues.empty()) {
            throw new EmptyStackException(getClass().getName());
        }
        context.stackedValues.pop();
    }
}
