package com.example.calculator.models.operations;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.example.calculator.exceptions.OperationArgsValidationException;
import com.example.calculator.exceptions.OperationException;
import com.example.calculator.models.containers.CalculatorContext;

public abstract class AbstractOperation<ValueType> implements FactoryEntityInterface<ValueType> {
    
    protected abstract List<Class<?>> getArgsFormat();

    protected List<Object> validateArgs(String argsLine) throws OperationArgsValidationException {
        
        List<Object> args = new ArrayList<Object>();
        List<String> stringArgs = new ArrayList<String>();

        if (argsLine != null && !argsLine.isEmpty()) {
            stringArgs = Arrays.asList(argsLine.split(" "));
        }

        if (stringArgs.size() != getArgsFormat().size()) {
            throw new OperationArgsValidationException(this.getClass().getName());
        }

        for (int i = 0; i < stringArgs.size(); i++) {
            try {
                if (getArgsFormat().get(i) != String.class) {
                    Method valueOf = getArgsFormat().get(i).getMethod("valueOf", String.class); 
                    args.add(valueOf.invoke(getArgsFormat().get(i), stringArgs.get(i)));
                } 
                else {
                    args.add(stringArgs.get(i));
                }
            } catch (ClassCastException e) {
                throw new OperationArgsValidationException(this.getClass().getName());
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        }
        return args;
    };

    public abstract void apply(CalculatorContext<ValueType> context, String argsLine) throws OperationException;

}
