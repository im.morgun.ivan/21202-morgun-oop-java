package com.example.calculator.models.operations.data_operations;

import com.example.calculator.exceptions.EmptyStackException;
import com.example.calculator.exceptions.OperationArgsValidationException;
import com.example.calculator.models.containers.CalculatorContext;
import com.example.calculator.models.operations.EmptyArgsOperation;

public class PrintOperation extends EmptyArgsOperation<Double> {

    @Override
    public void apply(CalculatorContext<Double> context, String argsLine) throws OperationArgsValidationException, EmptyStackException {
        validateArgs(argsLine);
        try {
            System.out.println(context.stackedValues.peek());
        } catch (Exception e) {
            throw new EmptyStackException(getClass().getName());
        }
    }

}
