package com.example.calculator.models.containers;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class CalculatorContext<ValueType> {
    
    public Stack<ValueType> stackedValues = new Stack<>();
    public Map<String, ValueType> definedValues = new HashMap<>();
    
}
