package com.example.calculator.services.factory;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.regex.MatchResult;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.example.calculator.exceptions.EntityClassCastException;
import com.example.calculator.exceptions.EntityClassNotFoundException;
import com.example.calculator.models.operations.AbstractOperation;

public class OperationFactory<OperationValueType> extends AbstractFactory<AbstractOperation<OperationValueType>, OperationValueType> {

    private static final Logger LOGGER = Logger.getLogger(OperationFactory.class.getName());
    private static final String CONFIG_PATTERN = "([\\w,\\+,\\-,\\/,\\*]+)\\s([\\w,\\.]+)"; 

    public OperationFactory(String operationsConfigFile) {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(operationsConfigFile)));
            String line;
            while ((line = reader.readLine()) != null) {
                Scanner scanner = new Scanner(line);
                if (scanner.findInLine(CONFIG_PATTERN) != null) {
                    MatchResult result = scanner.match(); 
                    registerEntity(result.group(1), result.group(2));
                }
                scanner.close();
            }
        } catch (FileNotFoundException e) {
            LOGGER.log(Level.WARNING, "Operation factory config file wasn't found", e);
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, "IOException was caught while reading operation factory config file", e);
        } catch (EntityClassNotFoundException e) {
            LOGGER.log(Level.WARNING, "Class from operation factory config file wasn't found", e);
        } catch (EntityClassCastException e) {
            LOGGER.log(Level.WARNING, "Class from operation factory config file can't be casted to required class", e);
        }
    }

}
