package com.example.calculator.services.factory;

import java.lang.reflect.InvocationTargetException;

import com.example.calculator.exceptions.EntityClassCastException;
import com.example.calculator.exceptions.EntityClassNotFoundException;
import com.example.calculator.exceptions.UnknownFactoryEntityCallName;

public interface FactoryInterface<EntityType> {

    public abstract void registerEntity(String entityCallName, String entityClassName) throws EntityClassNotFoundException, EntityClassCastException;
    public EntityType buildEntity(String entityCallName) throws UnknownFactoryEntityCallName, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException;

}
