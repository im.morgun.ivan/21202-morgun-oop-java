package com.example.calculator.services.factory;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.HashMap;

import com.example.calculator.exceptions.EntityClassCastException;
import com.example.calculator.exceptions.EntityClassNotFoundException;
import com.example.calculator.exceptions.UnknownFactoryEntityCallName;
import com.example.calculator.models.operations.FactoryEntityInterface;

public abstract class AbstractFactory<EntityType extends FactoryEntityInterface<EntityGenericType>, EntityGenericType> implements FactoryInterface<EntityType> {

    private Map<String, Class<EntityType>> entityMap = new HashMap<>(); 

    @Override
    public void registerEntity(String entityCallName, String entityClassName) throws EntityClassNotFoundException, EntityClassCastException {
        try {
            entityMap.put(entityCallName, (Class<EntityType>) Class.forName(entityClassName));
        } catch (ClassNotFoundException e) {
            throw new EntityClassNotFoundException(entityClassName);
        } catch (ClassCastException e) {
            throw new EntityClassCastException(entityClassName);
        }
    }

    @Override
    public EntityType buildEntity(String entityCallName) throws UnknownFactoryEntityCallName, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
        if (entityMap.get(entityCallName) == null) {
            throw new UnknownFactoryEntityCallName(entityCallName);
        }
        EntityType entity = entityMap.get(entityCallName).getDeclaredConstructor().newInstance();
        return entity;
    } 

}
