package com.example.calculator.services;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.example.calculator.exceptions.EmptyStackException;
import com.example.calculator.exceptions.NoDefinedValueException;
import com.example.calculator.exceptions.OperationArgsValidationException;
import com.example.calculator.exceptions.UnknownFactoryEntityCallName;
import com.example.calculator.models.containers.CalculatorContext;
import com.example.calculator.models.operations.AbstractOperation;
import com.example.calculator.services.factory.OperationFactory;

public class Calculator<ValueType> {

    private static final Logger LOGGER = Logger.getLogger(Calculator.class.getName());

    private CalculatorContext<ValueType> context = new CalculatorContext<>();
    private OperationFactory<ValueType> operationFactory;

    public Calculator(String operationDataPath) {
        operationFactory = new OperationFactory<ValueType>(operationDataPath);
    }

    public CalculatorContext<ValueType> getContext() {
        return context;
    }

    public void applyOperation(String operationName, String operationArgsLine) {
        try {
            AbstractOperation<ValueType> operation = operationFactory.buildEntity(operationName);
            operation.apply(context, operationArgsLine);

            LOGGER.log(Level.INFO, String.format("Applying %s operation", operationName));
        } catch (UnknownFactoryEntityCallName e) {
            LOGGER.log(Level.WARNING, "Operation isn't registered in operation factory", e);
        } catch (OperationArgsValidationException e) {
            LOGGER.log(Level.WARNING, "Operation args don't match required format", e);
        } catch (NoDefinedValueException e) {
            LOGGER.log(Level.WARNING, "Defined value wasn't found", e);
        } catch (EmptyStackException e) {
            LOGGER.log(Level.WARNING, "Can't pop/peek", e);
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "Exception was caught", e);
        }
    }

}
