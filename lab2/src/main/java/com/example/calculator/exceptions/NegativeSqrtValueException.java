package com.example.calculator.exceptions;

public class NegativeSqrtValueException extends MathException {
    
    public NegativeSqrtValueException(String operationName) {
        super(operationName, "negative value was passed to sqrt operation");
    }

}
