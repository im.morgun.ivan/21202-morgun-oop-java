package com.example.calculator.exceptions;

public class UnknownFactoryEntityCallName extends Exception {
    
    public UnknownFactoryEntityCallName(String operationName) {
        super("Unknown operation with name " + operationName);
    }

}
