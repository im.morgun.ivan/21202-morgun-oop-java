package com.example.calculator.exceptions;

public class EntityClassCastException extends Exception {
    
    public EntityClassCastException(String entityClassName) {
        super("Can't cast " + entityClassName + " to Class<EntityType>");
    }

}
