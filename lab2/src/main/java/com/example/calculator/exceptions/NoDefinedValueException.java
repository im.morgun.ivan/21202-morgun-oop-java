package com.example.calculator.exceptions;

public class NoDefinedValueException extends OperationException {
    
    public NoDefinedValueException(String operationName, String valueName) {
        super(operationName, "defined value " + valueName + " not found");
    }

}
