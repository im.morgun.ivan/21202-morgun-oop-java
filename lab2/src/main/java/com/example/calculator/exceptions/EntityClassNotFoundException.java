package com.example.calculator.exceptions;

public class EntityClassNotFoundException extends Exception {
    
    public EntityClassNotFoundException(String entityClassName) {
        super("No entity class with name " + entityClassName);
    }

}
