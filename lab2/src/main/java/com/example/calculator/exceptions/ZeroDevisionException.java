package com.example.calculator.exceptions;

public class ZeroDevisionException extends MathException {
    
    public ZeroDevisionException(String operationName) {
        super(operationName, "zero devision");
    }

}
