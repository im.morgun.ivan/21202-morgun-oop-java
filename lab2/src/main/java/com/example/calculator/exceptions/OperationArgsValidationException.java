package com.example.calculator.exceptions;

public class OperationArgsValidationException extends OperationException {
    
    public OperationArgsValidationException(String operationName) {
        super(operationName, "args validation exception");
    }

}
