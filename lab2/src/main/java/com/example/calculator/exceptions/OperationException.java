package com.example.calculator.exceptions;

public class OperationException extends Exception {
    
    public OperationException(String operationName, String description) {
        super("Exception in operation " + operationName + ": " + description);
    }

}
