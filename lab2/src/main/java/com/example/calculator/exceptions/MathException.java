package com.example.calculator.exceptions;

public class MathException extends OperationException {

    public MathException(String operationName, String description) {
        super(operationName, description);
    }

}
