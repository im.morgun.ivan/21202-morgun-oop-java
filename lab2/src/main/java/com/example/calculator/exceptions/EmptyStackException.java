package com.example.calculator.exceptions;

public class EmptyStackException extends OperationException {
    
    public EmptyStackException(String operationName) {
        super(operationName, "stack is empty, can't pop/peek");
    }

}
