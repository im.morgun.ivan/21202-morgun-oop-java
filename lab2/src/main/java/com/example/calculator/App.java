package com.example.calculator;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.regex.MatchResult;

import com.example.calculator.services.Calculator;

public class App {

    private static final Logger LOGGER = Logger.getLogger(App.class.getName());
    private static final String OPERATION_REGEX_PATTERN = "^([\\w,\\+,\\-,\\/,\\*]+)\\s*(.*)$";

    private static void redirectLogsToFile(boolean removeConsole) {
        try {
            Logger rootLogger = Logger.getLogger("");
            FileHandler fileHandler = new FileHandler("app.log");
            fileHandler.setFormatter(new SimpleFormatter());
            rootLogger.addHandler(fileHandler);

            if (removeConsole) {
                rootLogger.removeHandler(rootLogger.getHandlers()[0]);
                rootLogger.setLevel(Level.ALL);
            }
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, "IOException was caught while opening logs file", e);
        } 
    }

    public static void main( String[] args ) {

        redirectLogsToFile(false);
        
        Calculator<Double> calculator = new Calculator<>("operation_description.txt");

        InputStream inputStream = System.in;
        if (args.length == 1) {
            try {
                inputStream = new FileInputStream(args[0]);
                LOGGER.log(Level.FINE, "Program mode switched to file");
            } catch (FileNotFoundException e) {
                LOGGER.log(Level.WARNING, "Input file could not be found", e);
            }
        }

        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        try {
            String line;
            while ((line = reader.readLine()) != null) {
                Scanner scanner = new Scanner(line);
                if (scanner.findInLine(OPERATION_REGEX_PATTERN) != null) {
                    MatchResult result = scanner.match(); 
                    calculator.applyOperation(result.group(1), result.group(2));
                }
                scanner.close();
            }
        } catch (IOException e) { 
            LOGGER.log(Level.WARNING, "IOException was caught", e);
        }
    }

}
