package com.example.calculator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

import org.junit.Before;
import org.junit.Test;

import com.example.calculator.models.containers.CalculatorContext;
import com.example.calculator.models.operations.data_operations.DefineOperation;
import com.example.calculator.models.operations.data_operations.PopOperation;
import com.example.calculator.models.operations.data_operations.PushOperation;
import com.example.calculator.models.operations.math_operations.AdditionOperation;
import com.example.calculator.models.operations.math_operations.DivisionOperation;
import com.example.calculator.models.operations.math_operations.MultiplicationOperation;
import com.example.calculator.models.operations.math_operations.SqrtOperation;
import com.example.calculator.models.operations.math_operations.SubtractionOperation;

public class OperationsTest {

    public static CalculatorContext<Double> calculatorContext = new CalculatorContext<>();
    
    @Before
    public void clearData() {
        calculatorContext.definedValues.clear();
        calculatorContext.stackedValues.clear();
    }

    @Test
    public void testOperation_Define() {

        DefineOperation defineOperation = new DefineOperation();
        try {
            defineOperation.apply(calculatorContext, "a 100.5");
            defineOperation.apply(calculatorContext, "b 47.245");
        } catch (Exception e) {
            fail("Unexpected exception: " + e.getMessage());
        }

        assertEquals(100.5, calculatorContext.definedValues.get("a"), 1e-4);
        assertEquals(47.245, calculatorContext.definedValues.get("b"), 1e-4);

        try {
            defineOperation.apply(calculatorContext, "b 1");
        } catch (Exception e) {
            fail("Unexpected exception: " + e.getMessage());
        }

        assertEquals(1, calculatorContext.definedValues.get("b"), 1e-4);
    }

    @Test
    public void testOperation_Pop() {

        PopOperation popOperation = new PopOperation();

        calculatorContext.stackedValues.add(1.0);
        calculatorContext.stackedValues.add(2.0);
        calculatorContext.stackedValues.add(3.0);

        Stack<Double> expected = new Stack<Double>() {
            {
                push(1.0);
                push(2.0);
                push(3.0);
            }
        };

        for (int i = 0; i < 3; i++) {
            try {
                popOperation.apply(calculatorContext, null);
            } catch (Exception e) {
                fail("Unexpected exception: " + e.getMessage());
            }
            expected.pop();
            assertEquals(calculatorContext.stackedValues, expected);
        }
    }

    @Test
    public void testOperation_Push() {

        PushOperation pushOperation = new PushOperation();

        calculatorContext.definedValues.put("a", 100.0);
        calculatorContext.definedValues.put("b", 10.0);

        try {
            pushOperation.apply(calculatorContext, "a");
            pushOperation.apply(calculatorContext, "b");
            pushOperation.apply(calculatorContext, "a");
        } catch (Exception e) {
            fail("Unexpected exception" + e.getMessage());
        }

        Stack<Double> expected = new Stack<Double>() {
            {
                push(100.0);
                push(10.0);
                push(100.0);
            }
        };
        assertEquals(calculatorContext.stackedValues, expected);
    }

    @Test
    public void testOperation_Addition() {

        AdditionOperation additionOperation = new AdditionOperation();

        calculatorContext.stackedValues.push(100.5);
        calculatorContext.stackedValues.push(57.43);
        calculatorContext.stackedValues.push(-25.0);
        calculatorContext.stackedValues.push(35.5);

        List<Double> expected = new ArrayList<Double>() {
            {
                add(10.5);
                add(67.93);
                add(168.43);
            }
        };

        for (int i = 0; i < 3; i++) {
            try {
                additionOperation.apply(calculatorContext, null);
            } catch (Exception e) {
                fail("Unexpected exception" + e.getMessage());
            }
            assertEquals(calculatorContext.stackedValues.peek(), expected.get(i), 1e-4);
        }
    }

    @Test
    public void testOperation_Subtraction() {
        
        SubtractionOperation subtractionOperation = new SubtractionOperation();

        calculatorContext.stackedValues.push(100.5);
        calculatorContext.stackedValues.push(57.43);
        calculatorContext.stackedValues.push(35.5);
        calculatorContext.stackedValues.push(-25.0);

        List<Double> expected = new ArrayList<Double>() {
            {
                add(60.5);
                add(-3.07);
                add(103.57);
            }
        };

        for (int i = 0; i < 3; i++) {
            try {
                subtractionOperation.apply(calculatorContext, null);
            } catch (Exception e) {
                fail("Unexpected exception" + e.getMessage());
            }
            assertEquals(calculatorContext.stackedValues.peek(), expected.get(i), 1e-4);
        }
    }

    @Test
    public void testOperation_Multiplication() {
        
        MultiplicationOperation multiplicationOperation = new MultiplicationOperation();

        calculatorContext.stackedValues.push(-0.0067);
        calculatorContext.stackedValues.push(-57.43);
        calculatorContext.stackedValues.push(35.5);
        calculatorContext.stackedValues.push(25.0);

        List<Double> expected = new ArrayList<Double>() {
            {
                add(887.5);
                add(-50969.125);
                add(341.4931375);
            }
        };

        for (int i = 0; i < 3; i++) {
            try {
                multiplicationOperation.apply(calculatorContext, null);
            } catch (Exception e) {
                fail("Unexpected exception" + e.getMessage());
            }
            assertEquals(calculatorContext.stackedValues.peek(), expected.get(i), 1e-4);
        }
    }

    @Test
    public void testOperation_Division() {
         
        DivisionOperation divisionOperation = new DivisionOperation();

        calculatorContext.stackedValues.push(-100.5);
        calculatorContext.stackedValues.push(-57.43);
        calculatorContext.stackedValues.push(35.5);
        calculatorContext.stackedValues.push(25.0);

        List<Double> expected = new ArrayList<Double>() {
            {
                add(1.42);
                add(-40.443661971);
                add(2.4849381856);
            }
        };

        for (int i = 0; i < 3; i++) {
            try {
                divisionOperation.apply(calculatorContext, null);
            } catch (Exception e) {
                fail("Unexpected exception" + e.getMessage());
            }
            assertEquals(calculatorContext.stackedValues.peek(), expected.get(i), 1e-4);
        }
    }

    @Test
    public void testOperation_Sqrt() {
         
        SqrtOperation sqrtOperation = new SqrtOperation();

        calculatorContext.stackedValues.push(20905.2435);

        List<Double> expected = new ArrayList<Double>() {
            {
                add(144.586456834);
                add(12.024410872);
            }
        };

        for (int i = 0; i < 2; i++) {
            try {
                sqrtOperation.apply(calculatorContext, null);
            } catch (Exception e) {
                fail("Unexpected exception" + e.getMessage());
            }
            assertEquals(calculatorContext.stackedValues.peek(), expected.get(i), 1e-4);
        }
    }

}
