package src.notwizzard;

import java.awt.Dimension;
import java.awt.event.WindowAdapter;

import javax.swing.JFrame;

import src.notwizzard.factory.Factory;
import src.notwizzard.ui.MainView;

public class App {
    public static void main(String[] args) {

        Factory factory = new Factory();
        factory.start();

        JFrame frame = new JFrame();
        frame.setPreferredSize(new Dimension(800, 600));
        frame.add(new MainView(factory));
        frame.pack();
        frame.setVisible(true);

    }
}
