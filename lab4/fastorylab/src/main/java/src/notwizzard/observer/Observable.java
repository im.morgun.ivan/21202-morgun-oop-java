package src.notwizzard.observer;

import java.util.HashSet;
import java.util.Set;

public class Observable {

    private Set<Observer> observers = new HashSet<>();

    public boolean addObserver(Observer observer) {
        return observers.add(observer);
    }

    public boolean removeObserver(Observer observer) {
        if (observers.isEmpty()) {
            return true;
        }

        return observers.remove(observer);
    }

    public void notifyObservers(Context context) {
        if (observers.isEmpty()) {
            return;
        }

        for (Observer observer : observers) {
            observer.activate(context);
        }
    }

}
