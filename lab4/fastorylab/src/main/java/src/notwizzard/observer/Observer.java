package src.notwizzard.observer;

public interface Observer {

    public void activate(Context context);

}
