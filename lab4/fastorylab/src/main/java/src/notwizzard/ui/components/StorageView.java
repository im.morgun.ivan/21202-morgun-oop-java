package src.notwizzard.ui.components;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import src.notwizzard.factory.storage.Storage;
import src.notwizzard.factory.storage.StorageContext;
import src.notwizzard.observer.Context;
import src.notwizzard.observer.Observer;

public class StorageView extends JPanel implements Observer {

    private static String LABEL_FORMAT = "%s: %d";

    private final JLabel storageTitle;
    private final JLabel currentItemCount;
    private final JLabel totalItemCount;

    public StorageView(Storage<?> storage, String storageTitle) {
        storage.addObserver(this);
        this.storageTitle = new JLabel(storageTitle);
        this.currentItemCount = new JLabel(String.format(LABEL_FORMAT, "Current items", storage.getCurrentItemCount()));
        this.totalItemCount = new JLabel(String.format(LABEL_FORMAT, "Total items", storage.getTotalItemCount()));
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        add(this.storageTitle);
        add(currentItemCount);
        add(totalItemCount);
    }

    public void updateCurrentItemCount(int currentItemCount) {
        this.currentItemCount.setText(String.format(LABEL_FORMAT, "Current items", currentItemCount));
    }

    public void updateTotalItemCount(int totalItemCount) {
        this.totalItemCount.setText(String.format(LABEL_FORMAT, "Total items", totalItemCount));
    }

    @Override
    public void activate(Context context) {
        if (!context.getClass().equals(StorageContext.class)) {
            return;
        }

        StorageContext storageContext = (StorageContext) context;
        updateCurrentItemCount(storageContext.getCurrentItemCount());
        updateTotalItemCount(storageContext.getTotalItemCount());
    }

}
