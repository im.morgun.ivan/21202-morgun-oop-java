package src.notwizzard.ui.components;

import src.notwizzard.factory.base.WithDelay;
import src.notwizzard.threadpool.ThreadPool;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ThreadsView extends JPanel {

    private final ThreadPool threadPool;
    private final JLabel threadsLabel;
    private final JButton increaseButton;
    private final JButton decreaseButton;

    public ThreadsView(ThreadPool threadPool) {
        this.threadPool = threadPool;
        this.threadsLabel = new JLabel(String.format("%d", threadPool.getThreadsSize()));
        this.increaseButton = new JButton("+");
        this.decreaseButton = new JButton("-");
        this.increaseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                threadPool.addThread();
                threadsLabel.setText(String.format("%d", threadPool.getThreadsSize()));
            }
        });
        this.decreaseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                threadPool.removeThread();
                threadsLabel.setText(String.format("%d", threadPool.getThreadsSize()));
            }
        });
        setLayout(new FlowLayout(FlowLayout.RIGHT));
        add(new JLabel("Current threads number "));
        add(threadsLabel);
        add(increaseButton);
        add(decreaseButton);
    }

}
