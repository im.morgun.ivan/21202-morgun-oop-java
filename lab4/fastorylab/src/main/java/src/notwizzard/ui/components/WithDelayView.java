package src.notwizzard.ui.components;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import java.awt.FlowLayout;
import src.notwizzard.factory.base.WithDelay;

public class WithDelayView extends JPanel {

    private final WithDelay withDelayObject;
    private final JLabel delayLabel;
    private final JSlider delaySlider;

    public WithDelayView(WithDelay withDelayObject, String title) {
        this.withDelayObject = withDelayObject;
        this.delayLabel = new JLabel(String.format("%d ms", withDelayObject.getDelay()));
        this.delaySlider = new JSlider(withDelayObject.getMinimumDelay(), withDelayObject.getMaximumDelay(),
                withDelayObject.getDelay());
        this.delaySlider.addChangeListener((e) -> {
            this.withDelayObject.setDelay(delaySlider.getValue());
            this.delayLabel.setText(String.format("%d ms", this.withDelayObject.getDelay()));
        });
        setLayout(new FlowLayout(FlowLayout.RIGHT));
        add(new JLabel(title));
        add(delayLabel);
        add(delaySlider);
    }

}
