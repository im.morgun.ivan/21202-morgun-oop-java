package src.notwizzard.ui;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import java.awt.*;

import src.notwizzard.factory.Factory;
import src.notwizzard.factory.components.Accessory;
import src.notwizzard.factory.dealer.Dealer;
import src.notwizzard.factory.supplier.Supplier;
import src.notwizzard.ui.components.*;

public class MainView extends JPanel {

    public MainView(Factory factory) {
        setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        createViews(factory);
    }

    private void createViews(Factory factory) {
        Container storagesContainer = new Container();
        Container leftSideContainer = new Container();
        Container delayControlsContainer = new Container();
        Container wrapperContainer = new Container();
        Container threadsControlContainer = new Container();

        storagesContainer.add(new StorageView(factory.getBodyStorage(), "Body storage"));
        storagesContainer.add(new StorageView(factory.getEngineStorage(), "Engine storage"));
        storagesContainer.add(new StorageView(factory.getAccessoryStorage(), "Accessory storage"));
        storagesContainer.add(new StorageView(factory.getCarStorage(), "Car storage"));

        delayControlsContainer.add(new WithDelayView(factory.getBodySupplier(), "Body creation time"));
        delayControlsContainer.add(new WithDelayView(factory.getEngineSupplier(), "Engine creation time"));
        for (Supplier<Accessory> supplier : factory.getAccessorySuppliers())
            delayControlsContainer.add(new WithDelayView(supplier, "Accessory creation time"));
        for (Dealer dealer : factory.getDealers()) {
            delayControlsContainer.add(new WithDelayView(dealer, "Sale time"));
        }

        threadsControlContainer.add(new ThreadsView(factory.getWorkerController().getThreadPool()));

        storagesContainer.setLayout(new BoxLayout(storagesContainer, BoxLayout.Y_AXIS));
        threadsControlContainer.setLayout(new BoxLayout(threadsControlContainer, BoxLayout.Y_AXIS));
        leftSideContainer.setLayout(new BoxLayout(leftSideContainer, BoxLayout.Y_AXIS));
        delayControlsContainer.setLayout(new BoxLayout(delayControlsContainer, BoxLayout.Y_AXIS));
        wrapperContainer.setLayout(new BoxLayout(wrapperContainer, BoxLayout.X_AXIS));

        leftSideContainer.add(storagesContainer);

        wrapperContainer.add(leftSideContainer);
        wrapperContainer.add(delayControlsContainer);
        wrapperContainer.add(threadsControlContainer);

        this.add(wrapperContainer);
    }

}
