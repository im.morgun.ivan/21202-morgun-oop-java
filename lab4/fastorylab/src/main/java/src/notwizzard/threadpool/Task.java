package src.notwizzard.threadpool;

public interface Task {

    public void perform();

}
