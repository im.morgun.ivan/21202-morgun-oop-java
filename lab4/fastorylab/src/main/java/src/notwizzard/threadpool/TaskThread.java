package src.notwizzard.threadpool;

import java.util.concurrent.BlockingQueue;

import lombok.Setter;

public class TaskThread extends Thread {

    @Setter
    private BlockingQueue<Task> taskQueue;
    private volatile boolean stopped = false;

    @Override
    public void run() {
        while (!stopped) {
            System.out.println(this);
            if (taskQueue == null) {
                continue;
            }

            try {
                Task newTask = taskQueue.take();
                newTask.perform();
            } catch (InterruptedException e) {
                return;
            }
        }
    }

    public void kill() {
        interrupt();
        stopped = true;
    }

}
