package src.notwizzard.threadpool;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import src.notwizzard.factory.worker.Worker;

@Slf4j
@NoArgsConstructor
public class ThreadPool {

    private final Queue<TaskThread> threads = new LinkedList<>();
    private final BlockingQueue<Task> tasks = new LinkedBlockingQueue<>();

    public int getTasksSize() {
        return tasks.size();
    }

    public int getThreadsSize() {
        return threads.size();
    }

    public boolean isTaskQueueEmpty() {
        return tasks.size() == 0;
    }

    public boolean addTask(Task task) {
        return tasks.offer(task);
    }

    public void addThread(TaskThread thread) {
        thread.setTaskQueue(tasks);
        threads.add(thread);
    }

    public void addThread() {
        TaskThread thread = new Worker();
        thread.setTaskQueue(tasks);
        threads.add(thread);
        thread.start();
    }

    public void removeThread() {
        if (threads.isEmpty()) {
            return;
        }
        TaskThread thread = threads.poll();
        try {
            thread.kill();
        } catch (Exception e) {
            log.error(String.format("%s %s", e.getMessage(), e));
        }
    }

    public void start() {
        for (TaskThread thread : threads) {
            thread.start();
        }
    }

    public void stop() {
        for (TaskThread thread : threads) {
            thread.kill();
        }
    }

}
