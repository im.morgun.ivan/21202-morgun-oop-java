package src.notwizzard.factory.worker;

import java.util.UUID;

import src.notwizzard.threadpool.TaskThread;

public class Worker extends TaskThread {

    private final UUID ID = UUID.randomUUID();

    @Override
    public String toString() {
        return String.format("Worker <%s>", ID);
    }

}
