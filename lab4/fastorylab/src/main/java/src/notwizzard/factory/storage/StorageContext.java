package src.notwizzard.factory.storage;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import src.notwizzard.observer.Context;

@Getter
@RequiredArgsConstructor
public class StorageContext implements Context {

    private final int currentItemCount;
    private final int totalItemCount;

}
