package src.notwizzard.factory.supplier;

import java.lang.reflect.InvocationTargetException;
import java.util.UUID;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import src.notwizzard.factory.base.WithDelay;
import src.notwizzard.factory.components.Item;
import src.notwizzard.factory.storage.Storage;

@Slf4j
@RequiredArgsConstructor
public class Supplier<T extends Item> extends Thread implements WithDelay {

    private final UUID ID = UUID.randomUUID();

    private final Storage<T> storage;
    private final Class<T> itemClass;
    private Integer creationTime = 100;

    @Override
    public void setDelay(Integer delay) {
        creationTime = delay;
    }

    @Override
    public void run() {
        while (isAlive()) {
            if (creationTime <= 0) {
                log.warn("Creation time is zero or negative");
                continue;
            }

            T item;
            try {
                Thread.sleep(creationTime);
                item = itemClass.getDeclaredConstructor().newInstance();
            } catch (InterruptedException | IllegalAccessException | InvocationTargetException | InstantiationException
                    | NoSuchMethodException e) {
                return;
            }

            storage.putItem(item);
            log.info(String.format("%s exported %s", this, item));
        }
    }

    @Override
    public String toString() {
        return String.format("Supplier <%s>", ID);
    }

    @Override
    public Integer getMaximumDelay() {
        return 10000;
    }

    @Override
    public Integer getMinimumDelay() {
        return 0;
    }

    @Override
    public Integer getDelay() {
        return creationTime;
    }

}
