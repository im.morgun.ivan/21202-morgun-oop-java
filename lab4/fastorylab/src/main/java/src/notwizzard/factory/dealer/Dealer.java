package src.notwizzard.factory.dealer;

import java.util.UUID;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import src.notwizzard.factory.base.WithDelay;
import src.notwizzard.factory.components.Car;
import src.notwizzard.factory.storage.Storage;

@Slf4j
@RequiredArgsConstructor
public class Dealer extends Thread implements WithDelay {

    private final UUID ID = UUID.randomUUID();

    private final Storage<Car> carStorage;
    private Integer saleTime = 100;

    @Override
    public void setDelay(Integer delay) {
        saleTime = delay;
    }

    @Override
    public void run() {
        while (isAlive()) {
            if (saleTime <= 0) {
                log.warn("Sale time is zero or negative");
                continue;
            }

            try {
                Thread.sleep(saleTime);
            } catch (InterruptedException e) {
                return;
            }

            Car car = carStorage.takeItem();
            if (car != null) {
                log.info(String.format("%s sold %s | %s %s %s", this, car, car.getBody(), car.getEngine(),
                        car.getAccessory()));
            }
        }
    }

    @Override
    public String toString() {
        return String.format("Dealer <%s>", ID);
    }

    @Override
    public Integer getMaximumDelay() {
        return 10000;
    }

    @Override
    public Integer getMinimumDelay() {
        return 0;
    }

    @Override
    public Integer getDelay() {
        return saleTime;
    }

}
