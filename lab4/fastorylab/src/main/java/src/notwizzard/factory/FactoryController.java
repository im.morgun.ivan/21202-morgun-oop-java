package src.notwizzard.factory;

import src.notwizzard.factory.components.Accessory;
import src.notwizzard.factory.components.Body;
import src.notwizzard.factory.components.Car;
import src.notwizzard.factory.components.Engine;
import src.notwizzard.factory.storage.Storage;
import src.notwizzard.factory.storage.StorageContext;
import src.notwizzard.factory.worker.CarOrder;
import src.notwizzard.factory.worker.WorkerController;
import src.notwizzard.observer.Context;
import src.notwizzard.observer.Observer;

public class FactoryController implements Observer {

    private final Storage<Engine> engineStorage;
    private final Storage<Body> bodyStorage;
    private final Storage<Accessory> accessoryStorage;
    private final Storage<Car> carStorage;
    private final WorkerController workerController;
    private final float optimalOccupancyRate;

    public FactoryController(Storage<Body> bodyStorage, Storage<Engine> engineStorage,
            Storage<Accessory> accessoryStorage, Storage<Car> carStorage,
            WorkerController workerController, float optimalOccupancyRate) {

        this.engineStorage = engineStorage;
        this.bodyStorage = bodyStorage;
        this.accessoryStorage = accessoryStorage;
        this.carStorage = carStorage;
        this.workerController = workerController;
        this.optimalOccupancyRate = optimalOccupancyRate;

        carStorage.addObserver(this);

        for (int i = 0; i < carStorage.getCapacity(); i++) {
            makeCarOrder();
        }
    }

    private void makeCarOrder() {
        CarOrder carOrder = new CarOrder(bodyStorage, engineStorage, accessoryStorage, carStorage);
        workerController.addTask(carOrder);
    }

    private void analizeCarStorageOccupancy(int currentOccupacyNumber) {
        int optimalOccupancyNumber = (int) (carStorage.getCapacity() * optimalOccupancyRate);
        int alreadyOrderedNumber = workerController.getCurrentQueuedTaskCount();

        if (currentOccupacyNumber + alreadyOrderedNumber < optimalOccupancyNumber) {
            int delta = optimalOccupancyNumber - alreadyOrderedNumber - currentOccupacyNumber;
            for (int i = 0; i < delta; i++) {
                makeCarOrder();
            }
        }
    }

    @Override
    public void activate(Context context) {
        if (!context.getClass().equals(StorageContext.class)) {
            return;
        }

        analizeCarStorageOccupancy(((StorageContext) context).getCurrentItemCount());
    }

}
