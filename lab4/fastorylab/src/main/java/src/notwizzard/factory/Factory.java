package src.notwizzard.factory;

import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.LoggerContext;

import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import src.notwizzard.factory.components.Accessory;
import src.notwizzard.factory.components.Body;
import src.notwizzard.factory.components.Car;
import src.notwizzard.factory.components.Engine;
import src.notwizzard.factory.config.FactorySettings;
import src.notwizzard.factory.dealer.Dealer;
import src.notwizzard.factory.storage.Storage;
import src.notwizzard.factory.supplier.Supplier;
import src.notwizzard.factory.worker.WorkerController;
import src.notwizzard.observer.Observer;

@Log4j2
public class Factory {

    @Getter
    private final Storage<Body> bodyStorage;
    @Getter
    private final Storage<Engine> engineStorage;
    @Getter
    private final Storage<Accessory> accessoryStorage;
    @Getter
    private final Storage<Car> carStorage;

    @Getter
    private final Supplier<Body> bodySupplier;
    @Getter
    private final Supplier<Engine> engineSupplier;
    @Getter
    private final List<Supplier<Accessory>> accessorySuppliers;

    @Getter
    private final List<Dealer> dealers;

    @Getter
    private final WorkerController workerController;
    private final FactoryController factoryController;

    public Factory() {

        if (!FactorySettings.isLoggerOn()) {
            turnOffLogs();
        }

        log.info("Starting factory creation");
        log.info("Creating storages");
        bodyStorage = new Storage<>(FactorySettings.getBodyStorageCapacity());
        engineStorage = new Storage<>(FactorySettings.getEngineStorageCapacity());
        accessoryStorage = new Storage<>(FactorySettings.getAccessoryStorageCapacity());
        carStorage = new Storage<>(FactorySettings.getCarStorageCapacity());

        log.info("Creating suppliers");
        bodySupplier = new Supplier<>(bodyStorage, Body.class);
        engineSupplier = new Supplier<>(engineStorage, Engine.class);
        accessorySuppliers = new ArrayList<>();
        for (int i = 0; i < FactorySettings.getAccessorySupplierCount(); i++) {
            accessorySuppliers.add(new Supplier<>(accessoryStorage, Accessory.class));
        }

        log.info("Creating workers");
        workerController = new WorkerController(FactorySettings.getWorkerCount());

        log.info("Creating factory controller");
        factoryController = new FactoryController(bodyStorage, engineStorage, accessoryStorage, carStorage,
                workerController, FactorySettings.getOptimalOccupancyRate());

        log.info("Creating dealers");
        dealers = new ArrayList<>();
        for (int i = 0; i < FactorySettings.getDealerCount(); ++i) {
            dealers.add(new Dealer(carStorage));
        }

        log.info("Factory is created");
    }

    public void start() {
        log.info("Start factory");
        engineSupplier.start();
        bodySupplier.start();
        for (Supplier<Accessory> accessorySupplier : accessorySuppliers) {
            accessorySupplier.start();
        }
        workerController.start();
        for (Dealer dealer : dealers) {
            dealer.start();
        }
    }

    public void stop() {
        log.info("Stop factory");
        for (Dealer dealer : dealers) {
            dealer.interrupt();
        }
        workerController.stop();
        bodySupplier.interrupt();
        engineSupplier.interrupt();
        for (Supplier<Accessory> accessorySupplier : accessorySuppliers) {
            accessorySupplier.interrupt();
        }
    }

    public void turnOffLogs() {
        LoggerContext loggerContext = (LoggerContext) LogManager.getContext(false);
        loggerContext.getRootLogger().setLevel(Level.OFF);
    }

    public void setBodyCreationTime(int creationTime) {
        bodySupplier.setDelay(creationTime);
    }

    public void setEngineCreationTime(int creationTime) {
        engineSupplier.setDelay(creationTime);
    }

    public void setAccessoryCreationTime(int creationTime) {
        for (Supplier<Accessory> accessorySupplier : accessorySuppliers) {
            accessorySupplier.setDelay(creationTime);
        }
    }

    public void setCarSaleTime(int saleTime) {
        for (Dealer dealer : dealers) {
            dealer.setDelay(saleTime);
        }
    }

    public void addEngineStorageObserver(Observer observer) {
        engineStorage.addObserver(observer);
    }

    public void addBodyStorageObserver(Observer observer) {
        bodyStorage.addObserver(observer);
    }

    public void addAccessoryStorageObserver(Observer observer) {
        accessoryStorage.addObserver(observer);
    }

    public void addCarStorageObserver(Observer observer) {
        carStorage.addObserver(observer);
    }

}
