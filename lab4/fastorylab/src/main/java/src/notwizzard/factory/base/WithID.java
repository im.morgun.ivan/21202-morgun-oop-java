package src.notwizzard.factory.base;

import java.util.UUID;

public class WithID {

    private final UUID ID = UUID.randomUUID();

    @Override
    public String toString() {
        return String.format("%s <%s>", this.getClass().getSimpleName(), ID);
    }
}
