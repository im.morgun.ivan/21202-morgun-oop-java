package src.notwizzard.factory.storage;

import java.util.ArrayDeque;
import java.util.UUID;

import lombok.Getter;
import src.notwizzard.factory.components.Item;
import src.notwizzard.observer.Observable;

public class Storage<T extends Item> extends Observable {

    private final UUID ID = UUID.randomUUID();

    private final ArrayDeque<T> items;
    @Getter
    private final int capacity;
    @Getter
    private int totalItemCount = 0;

    public Storage(int capacity) {
        this.capacity = capacity;
        this.items = new ArrayDeque<>(capacity);
    }

    public int getCurrentItemCount() {
        synchronized (items) {
            return items.size();
        }
    }

    private boolean isFull() {
        synchronized (items) {
            return items.size() >= capacity;
        }
    }

    private boolean isEmpty() {
        synchronized (items) {
            return items.isEmpty();
        }
    }

    public void putItem(T item) {
        if (item == null) {
            return;
        }

        synchronized (items) {
            while (isFull() && Thread.currentThread().isAlive()) {
                try {
                    items.wait();
                } catch (InterruptedException e) {
                    return;
                }
            }

            items.add(item);
            totalItemCount++;

            items.notifyAll();
            notifyObservers(new StorageContext(getCurrentItemCount(), totalItemCount));
        }
    }

    public T takeItem() {
        T item = null;

        synchronized (items) {
            while (isEmpty() && Thread.currentThread().isAlive()) {
                try {
                    items.wait();
                } catch (InterruptedException e) {
                    return item;
                }
            }

            item = items.remove();

            items.notifyAll();
            notifyObservers(new StorageContext(getCurrentItemCount(), totalItemCount));
        }

        return item;
    }

    @Override
    public String toString() {
        return String.format("Storage <%s>", ID);
    }

}
