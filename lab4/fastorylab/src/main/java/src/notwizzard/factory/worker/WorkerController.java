package src.notwizzard.factory.worker;

import lombok.Getter;
import src.notwizzard.observer.Context;
import src.notwizzard.observer.Observer;
import src.notwizzard.threadpool.Task;
import src.notwizzard.threadpool.ThreadPool;

public class WorkerController implements Observer {

    @Getter
    private final ThreadPool threadPool = new ThreadPool();
    @Getter
    private int totalPerformedTaskCount = 0;

    public WorkerController(int workerCount) {
        for (int i = 0; i < workerCount; i++) {
            threadPool.addThread(new Worker());
        }
    }

    public int getCurrentQueuedTaskCount() {
        return threadPool.getTasksSize();
    }

    public boolean addTask(Task task) {
        return threadPool.addTask(task);
    }

    public void start() {
        threadPool.start();
    }

    public void stop() {
        threadPool.stop();
    }

    @Override
    public synchronized void activate(Context context) {
        totalPerformedTaskCount++;
    }

}
