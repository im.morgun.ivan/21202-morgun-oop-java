package src.notwizzard.factory.base;

public interface WithDelay {

    public Integer getDelay();

    public void setDelay(Integer delay);

    public Integer getMaximumDelay();

    public Integer getMinimumDelay();

}
