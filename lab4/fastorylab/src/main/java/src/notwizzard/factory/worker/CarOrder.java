package src.notwizzard.factory.worker;

import lombok.RequiredArgsConstructor;
import src.notwizzard.factory.components.Accessory;
import src.notwizzard.factory.components.Body;
import src.notwizzard.factory.components.Car;
import src.notwizzard.factory.components.Engine;
import src.notwizzard.factory.storage.Storage;
import src.notwizzard.observer.Observable;
import src.notwizzard.threadpool.Task;

@RequiredArgsConstructor
public class CarOrder extends Observable implements Task {

    private final Storage<Body> bodyStorage;
    private final Storage<Engine> engineStorage;
    private final Storage<Accessory> accessoryStorage;
    private final Storage<Car> carStorage;

    @Override
    public void perform() {
        Body body = bodyStorage.takeItem();
        Engine engine = engineStorage.takeItem();
        Accessory accessory = accessoryStorage.takeItem();

        if (body == null || engine == null || accessory == null) {
            bodyStorage.putItem(body);
            engineStorage.putItem(engine);
            accessoryStorage.putItem(accessory);
            return;
        }

        Car car = new Car(body, engine, accessory);
        carStorage.putItem(car);
        notifyObservers(null);
    }

}
