package src.notwizzard.factory.config;

import java.io.InputStream;
import java.util.Properties;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.Level;

@Slf4j
public class FactorySettings {

    private static final String FILE_NAME = "factory.properties";
    private static final Properties settings;

    static {
        try {
            InputStream settingStream = FactorySettings.class.getClassLoader().getResourceAsStream(FILE_NAME);
            if (settingStream == null) {
                throw new NullPointerException("Properties file is not found");
            }

            settings = new Properties();
            settings.load(settingStream);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    private static Boolean parseBoolean(String toParse) {
        try {
            return Boolean.parseBoolean(toParse);
        } catch (NumberFormatException e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    private static Integer parseInt(String toParse) {
        try {
            return Integer.parseInt(toParse);
        } catch (NumberFormatException e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    private static Float parseFloat(String toParse) {
        try {
            return Float.parseFloat(toParse);
        } catch (NumberFormatException e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    @Getter
    private static final boolean loggerOn = parseBoolean(settings.getProperty("Logger.On"));

    @Getter
    private static final int bodyStorageCapacity = parseInt(settings.getProperty("Storage.Body.Capacity"));

    @Getter
    private static final int engineStorageCapacity = parseInt(settings.getProperty("Storage.Engine.Capacity"));

    @Getter
    private static final int accessoryStorageCapacity = parseInt(settings.getProperty("Storage.Accessory.Capacity"));

    @Getter
    private static final int carStorageCapacity = parseInt(settings.getProperty("Storage.Car.Capacity"));

    @Getter
    private static final int accessorySupplierCount = parseInt(settings.getProperty("Supplier.Accessory.Count"));

    @Getter
    private static final int workerCount = parseInt(settings.getProperty("Worker.Count"));

    @Getter
    private static final int dealerCount = parseInt(settings.getProperty("Dealer.Count"));

    @Getter
    private static final float optimalOccupancyRate = parseFloat(
            settings.getProperty("Storage.Car.OptimalOccupancyRate"));

}
