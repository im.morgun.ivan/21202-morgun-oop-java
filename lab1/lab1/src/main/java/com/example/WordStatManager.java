package com.example;

import java.util.Comparator;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;

public class WordStatManager {
    private Map<String, Integer> wordFrequency = new HashMap<>();
    private int wordCount = 0;

    public void addWord(String word) {
        wordCount++;
        Integer curWordFreq = 1;
        if (wordFrequency.containsKey(word)) {
            curWordFreq = wordFrequency.get(word);
            curWordFreq++;
        }
        wordFrequency.put(word, curWordFreq);
    }

    public Set<WordStatContainer> getWordStat() {
        Set<WordStatContainer> wordStat = new TreeSet<>(new Comparator<WordStatContainer>() {
            @Override
            public int compare(WordStatContainer c1, WordStatContainer c2) {
                int cmpRes = Integer.compare(c2.frequency, c1.frequency); 
                if (cmpRes == 0) {
                    return c1.word.compareTo(c2.word);
                }
                else {
                    return cmpRes;
                }
            }
        });

        for (Map.Entry<String, Integer> entry : wordFrequency.entrySet()) {
            String word = entry.getKey();
            Integer freq = entry.getValue();
            WordStatContainer curWordStat = new WordStatContainer();
            curWordStat.word = word;
            curWordStat.frequency = freq;
            curWordStat.freqPercentage = 100 * freq.doubleValue() / wordCount;
            wordStat.add(curWordStat);
        }
        return wordStat;
    }
}
