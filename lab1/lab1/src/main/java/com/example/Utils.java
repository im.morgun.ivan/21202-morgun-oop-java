package com.example;

import java.io.*;
import java.util.Set;

public class Utils {
    
    static public void PrintStat(String outputFileName, Set<WordStatContainer> wordStat) {
        try (FileWriter writer = new FileWriter(outputFileName)){
            for (WordStatContainer curWordStat : wordStat){
                writer.write(curWordStat.word + ", ");
                writer.write(curWordStat.frequency + ", ");
                writer.write(curWordStat.freqPercentage + "\n");
            }
        } catch (IOException e) {
            System.err.print("Error " + e + "when try write at file " + outputFileName);
        }
    }
}
