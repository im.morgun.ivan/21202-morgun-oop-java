package com.example;

import java.io.*;
import java.util.Set;

public class App 
{
    public static void main(String[] args)
    {
        if (args.length != 2) {
            System.err.println("Incorrect number of arguments (<input> <output> required)");
            return;
        }

        WordStatManager wordStatManager = new WordStatManager();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(args[0])))) {
            String curLine;
            while ((curLine = reader.readLine()) != null) {
                for (String curWord : curLine.split("[\\W]")) {
                    if (!curWord.isEmpty()) {
                        wordStatManager.addWord(curWord);
                    }
                }
            }
        } 
        catch (IOException e) {
            System.err.println("Error " + e + " while read file " + args[0]);
            return;
        }

        Set<WordStatContainer> wordStat = wordStatManager.getWordStat();
        Utils.PrintStat(args[1], wordStat);
    }
}
